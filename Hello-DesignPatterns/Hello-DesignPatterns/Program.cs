﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Runtime.CompilerServices;
using NStack;
using Terminal.Gui;

namespace Hello_DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Init();

            var window = SetupTopWindow();
            var menuBar = SetupMenuBar();

            Application.Top.Add(window);
            Application.Top.Add(menuBar);


            var textbox = SetupTextBox(menuBar, "");
            new List<View>
            {
                
                SetupListView(Enumerable.Range(1, 10).Select(n => $"List entry {n}").ToList(), textbox),
                textbox
            }.ForEach(view => window.Add(view));

            
            Application.Run();
        }

        private static TextField SetupTextBox(View prevViewElement, string content)
        {
            var textbox = new TextField(content);
            textbox.Y = prevViewElement.Y 
                        + 1;

            return textbox;
        }

        private static View SetupListView(List<string> items, TextField logTextField)
        {
            var listView = new ListView(items);
            listView.Y = logTextField.Y + 2;

            listView.SelectedChanged += () =>
            {
                var value = items[listView.SelectedItem];
                logTextField.Text = $"Selected: {value}";
            };

            return listView;
        }

        private static View SetupMenuBar()
        {
            var fileMenuItem = new MenuBarItem("File", new[]
            {
                new MenuItem(
                    "Quit",
                    "Quit the Application",
                    () => Quit()
                )
            });


            var menu = new MenuBar(new[]
            {
                fileMenuItem
            });

            return menu;
        }

        private static void Quit(int exitCode = 0)
        {
            Environment.Exit(exitCode);
        }


        private static View SetupTopWindow(bool hasMenuBar = false)
        {
            var assemblyName = Assembly.GetEntryAssembly()?.GetName().Name;
            var window = new Window(assemblyName)
            {
                // if needed, make space for the menuBar in row 0
                X = 0,
                Y = hasMenuBar ? 1 : 0,

                // By using Dim.Fill(), it will automatically resize without manual intervention
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };

            return window;
        }
    }
}