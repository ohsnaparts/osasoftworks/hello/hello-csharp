# Hello-Mediatr

This app demonstrates the positive effects of using CQRS and [Mediatr](https://github.com/jbogard/MediatR)

## Visuals

![](Screenshot_from_2021-12-23_15-56-09.jpg)

## Requirements v1.0

> You are sitting at a customer meeting

_Hey Tim! So our IT guys upstairs have been building this gigantic set of bash scripts and would really need a simple solution to log errors conveniently to our network service. Could you implement a little CLI tool?`_

It doesn't need much
* CLI program taking in 2 arguments
    1. severity
    2. message

## Requirements v2.0

> The tool is a complete success and people are celebrating every day. Logging is just so easy now....... until...

_Hey Tim! Love the tool! But we've noticed a few inconsistencies in our database. Since the severity isn't validated, our logging service is crashing everytime someone has a typo in their severity. Could you please add validation?
The severity can only be one of the following lowercase strings
* _information_
* _warning_
* _error_

## Imaginary future requirements

_Our RnD team at the other office is using gRPC very heavily. Could you make your logging service available to those as well?_ 

1. Add a gRPC endpoint
2. Log commands using gRPC

## Remarks

This use-case, in its simplest form, represents normal software development. The redefinition of 'single-use-case' features to be 'multi-use-case' features. A lot of flexibility is required to quickly react to this kind of request.
Without having to heavily restructure the code, additional features are to be introduced. Through the use of CQRS and Mediatr, this can easily be done.

* The logging / list logic is neatly wrapped into classes with no dependencies to their enviroments
* Therefore, it would make no difference who is calling the service (gRPC, REST, CLI, ...)


## Requirements

* System able to run .NET 6+
* See [.csproj file](./Hello-Mediatr.csproj) for the exact version to install

```sh
# Ubuntu
apt search dotnet-sdk
sudo apt install dotnet-sdk-6.0
```
