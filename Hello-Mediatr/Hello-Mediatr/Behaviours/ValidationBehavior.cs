using FluentValidation;
using MediatR;

namespace Hello_Mediatr.Behaviours;

public class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : notnull
{
    private readonly IValidator<TRequest>? _validator;

    public ValidationBehavior(IValidator<TRequest>? validator = null)
    {
        _validator = validator;
    }
    public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
    {
        if (_validator is null)
        {
            return await next();
        }

        var validationResult = await _validator.ValidateAsync(
            new ValidationContext<TRequest>(request), 
            cancellationToken
        );
        
        if (validationResult.IsValid)
        {
            return await next();
        }

        throw new ValidationException(validationResult.Errors);
    }
}