using FluentValidation;
using Hello_Mediatr.Domain.Entities;
using Hello_Mediatr.Domain.Enums;
using Hello_Mediatr.Store;
using MediatR;

namespace Hello_Mediatr.Mediatr.Commands;

public record LogMessageCommand(LogMessage Message) : IRequest<Guid>;


public class LogMessageCommandHandler : IRequestHandler<LogMessageCommand, Guid>
{
    private readonly ILogStore _logStore;

    public LogMessageCommandHandler(ILogStore logStore)
    {
        _logStore = logStore;
    }

    public async Task<Guid> Handle(LogMessageCommand request, CancellationToken cancellationToken)
    {
        var id = await _logStore.AddLogAsync(request.Message);
        return id;
    }
}

// ReSharper disable once UnusedType.Global
public class LogMessageCommandValidator : AbstractValidator<LogMessageCommand>
{
    public LogMessageCommandValidator()
    {
        RuleFor(request => request.Message.Time).NotEmpty();
        RuleFor(request => request.Message.Severity).NotEmpty();
        RuleFor(request => request.Message.Message).NotEmpty();
        RuleFor(request => request.Message.Severity)
            .IsEnumName(typeof(LogSeverity))
            .WithMessage((request, error) =>
                $"{error} is invalid. Valid values: '{string.Join(',', Enum.GetNames(typeof(LogSeverity)))}'");
    }
}