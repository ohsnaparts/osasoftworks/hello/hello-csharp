using Hello_Mediatr.Domain.Entities;
using Hello_Mediatr.Store;
using MediatR;

namespace Hello_Mediatr.Mediatr.Requests;


public record GetLogsQuery() : IRequest<ICollection<LogMessage>>;

public class GetLogsQueryHandler : IRequestHandler<GetLogsQuery, ICollection<LogMessage>>
{
    private readonly ILogStore _logStore;

    public GetLogsQueryHandler(ILogStore logStore)
    {
        _logStore = logStore;
    }
    
    public async Task<ICollection<LogMessage>> Handle(GetLogsQuery request, CancellationToken cancellationToken)
    {
        return await _logStore.GetAllAsync();
    }
}