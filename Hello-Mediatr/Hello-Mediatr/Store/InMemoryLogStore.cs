using Hello_Mediatr.Domain.Entities;

namespace Hello_Mediatr.Store;

public interface ILogStore
{
    Task<Guid> AddLogAsync(LogMessage logMessage);
    Task<ICollection<LogMessage>> GetAllAsync();
}

public class InMemoryLogStore : ILogStore
{
    private readonly Dictionary<Guid, LogMessage> _logs = new();

    public Task<Guid> AddLogAsync(LogMessage logMessage)
    {
        var id = Guid.NewGuid();
        _logs[id] = logMessage;

        return Task.FromResult(id);
    }

    public Task<ICollection<LogMessage>> GetAllAsync()
    {
        ICollection<LogMessage> logs = _logs.Values.ToList();
        return Task.FromResult(logs);
    }
}