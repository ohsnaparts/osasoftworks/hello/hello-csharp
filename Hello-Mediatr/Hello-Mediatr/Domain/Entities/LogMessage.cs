namespace Hello_Mediatr.Domain.Entities;

public record LogMessage(
    DateTimeOffset Time,
    string Severity,
    string Message
)
{
    public override string ToString() => $"[{Time} {Severity}]: {Message}";
}