namespace Hello_Mediatr.Domain.Enums;

public enum LogSeverity
{
    Information = 0,
    Warning = 1,
    Error = 2
}