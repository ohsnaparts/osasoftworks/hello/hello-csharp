﻿using System.Threading.Tasks;

namespace Hello_Language
{
    interface IRunnable
    {
        Task Run();
    }
}