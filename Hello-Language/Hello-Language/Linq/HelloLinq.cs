﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Language.Linq
{
    class HelloLinq : IRunnable
    {
        /// <summary>Tests if there are elements in the enumeration by creating an enumerator
        /// and trying to move its pointer to the first element</summary>
        /// <see href="https://github.com/dotnet/corefx/blob/master/src/System.Linq/src/System/Linq/AnyAll.cs"/>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>true if the enumerator was able to find an element</returns>
        private bool Any<T>(IEnumerable<T> enumerable)
        {
            return enumerable.Any();
        }

        public Task Run()
        {
            var elements = new List<int> {1, 2, 3};

            if (Any(elements))
            {
                elements.ForEach(Console.WriteLine);
            }

            return Task.CompletedTask;
        }
    }
}