﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hello_Language.Collections
{
    public class HelloYield : IRunnable
    {
        public Task Run()
        {
            // manually using enumerator to select single elements
            using (var colors = Colors().GetEnumerator())
            {
                Console.Write("First color: ");
                if(colors.MoveNext())
                    Console.WriteLine(colors.Current);

                Console.WriteLine("All other colors");
                while (colors.MoveNext())
                    Console.WriteLine($"  {colors.Current}");
            }

            Console.WriteLine();
            Console.WriteLine();
            var cnt = 0;


            // Foreach iterates over its enumerator
            foreach (var color in Colors())
                Console.WriteLine($"{cnt++}: {color}");

            return Task.CompletedTask;
        }


        private IEnumerable<string> Colors()
        {
            foreach (var color in Enum.GetNames(typeof(ConsoleColor)))
                yield return color;
        }
    }
}