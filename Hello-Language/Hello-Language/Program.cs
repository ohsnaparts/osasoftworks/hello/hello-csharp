﻿using System;
using System.Diagnostics.Tracing;
using System.Threading.Tasks;
using Hello_Language.Collections;
using Hello_Language.Linq;

namespace Hello_Language
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args != null && args.Length == 0)
            {
                Console.WriteLine("Argument 1 is missing: Please select an implementation to run!");
                Environment.Exit(1);
            }

            IRunnable runnable = default;
            switch (args?[0])
            {
                case nameof(HelloGetterSetter):
                    runnable = new HelloGetterSetter();
                    break;
                case nameof(HelloLinq):
                    runnable = new HelloLinq();
                    break;
                case nameof(HelloYield):
                    runnable = new HelloYield();
                    break;
                case nameof(HelloElvis):
                    runnable = new HelloElvis();
                    break;
                case default:
                    runnable = new 
            }

            await runnable?.Run();
        }
    }
}