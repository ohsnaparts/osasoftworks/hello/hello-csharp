﻿using System;
using System.Threading.Tasks;

namespace Hello_Language.Collections
{
    public class HelloElvis : IRunnable
    {
        public async Task Run()
        {
            var now = await FetchValue<string>(null)
                      ?? DateTime.Now.ToLongDateString();

            var yesterday = await FetchValue(
                DateTime.Today.AddDays(-1).ToLongDateString()
            );

            Console.WriteLine($"Now: ${now}");
            Console.WriteLine($"Yesterday: {yesterday}");
        }


        private async Task<TValue> FetchValue<TValue>(TValue value)
        {
            return await Task.FromResult(value);
        }
    }
}