﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hello_Language
{
    /// <summary>
    /// Tests the difference between
    ///   1: 'X {get;} = Y'
    ///   2: 'X => Y'
    /// Result: 1 assigns the value to the variable and makes it available through the getter
    ///         2 calculates the value directly in the getter, calling it multiple times
    /// </summary>
    class HelloGetterSetter : IRunnable
    {
        private static int _counter1 = 0;
        private static int _counter2 = 0;

        public int Counter1 { get; set; } = ++_counter1;
        public int Counter2 => ++_counter2;

        public Task Run()
        {
            Enumerable.Range(1, 10).ToList().ForEach(cnt =>
            {
                Console.WriteLine($"Counter 1: {Counter1}, Counter 2: {Counter2}");
            });

            return Task.CompletedTask;
        }
    }
}