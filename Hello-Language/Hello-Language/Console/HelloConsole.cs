﻿using System.Threading.Tasks;

namespace Hello_Language.Console
{
    public class HelloConsole : IRunnable
    {
        public Task Run()
        {
            return Task.CompletedTask;
        }
    }
}