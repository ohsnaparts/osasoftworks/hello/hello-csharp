﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using Prism.Regions;

namespace Hello_Prism.Regions
{
    public class StackPanelRegionAdapter : RegionAdapterBase<StackPanel>
    {
        public StackPanelRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory) 
            : base(regionBehaviorFactory)
        {
        }

        protected override void Adapt(IRegion region, StackPanel regionTarget)
        {
            // implementing the logic that takes the incoming view and adds it to the stack panel
            // add or remove the items from the stackpanel that are added or removed from this regions

            region.Views.CollectionChanged += (s, e) =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                    {
                        foreach (UIElement item in e.NewItems)
                            regionTarget.Children.Add(item);
                        break;
                    }
                    case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (UIElement item in e.OldItems)
                            regionTarget.Children.Remove(item);
                        break;
                    }
                }
            };
        }

        protected override IRegion CreateRegion()
        {
            return new Region();
        }
    }
}