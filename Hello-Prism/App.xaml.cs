﻿using Prism.Ioc;
using Hello_Prism.Views;
using System.Windows;
using System.Windows.Controls;
using Hello_Prism.Regions;
using ModuleA;
using Prism.DryIoc;
using Prism.Modularity;
using Prism.Regions;

namespace Hello_Prism
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            // Shell represents the main window
            // Inject MainWindow component and use it as the master page
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            //A module is a functional responsibility of your application. It groups together 
            //all the views, services and logic for a specific feature that can stand on its own.
            //In web-development this would be a web-component.
            //From within visual studio, modules are class-libraries that implement IModule
            //
            // Modules can be registered using multiple methods
            //  Code: very static
            //  App.config
            //  Disk/Directory
            //  XAML
            //  Custom
            
            base.ConfigureModuleCatalog(moduleCatalog);

            // Code based registration:
            moduleCatalog.AddModule<ModuleAModule>();
        }

        protected override void ConfigureRegionAdapterMappings(RegionAdapterMappings regionAdapterMappings)
        {
            base.ConfigureRegionAdapterMappings(regionAdapterMappings);

            regionAdapterMappings.RegisterMapping(
                typeof(StackPanel), 
                Container.Resolve<StackPanelRegionAdapter>()
            );
        }
    }
}