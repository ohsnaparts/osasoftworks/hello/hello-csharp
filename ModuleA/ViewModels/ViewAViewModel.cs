﻿using System;
using Prism.Commands;
using Prism.Mvvm;

namespace ModuleA.ViewModels
{
    public class ViewAViewModel : BindableBase
    {
        private string _title = "Hello from ViewAViewModel";
        private bool _canSave = true;


        ///<summary>
        /// ICommand binds a UI gesture to an action
        /// Execute(object) - 
        ///   CanExecute(object) - 
        ///          about whether or not the Execute(object) method is allowed to run
        ///          disables / enables element
        /// DelegateCommand : ICommand
        ///  Does not require an event handler because it uses delegate methods
        ///  Defined within a ViewModel
        /// </summary>
        public DelegateCommand ClickCommand { get; set; }


        public ViewAViewModel()
        {
            ClickCommand = new DelegateCommand(Execute)
                .ObservesCanExecute(() => CanSave);    // replaces CanExecute, watches for changes, notifies UI
        }

        /// <summary>Invoked when event is raised or command is executed</summary>
        private void Execute()
        {
            Title = $"{Title}.";

        }

        /// <summary>
        /// Returns whether or not the Execute delegate is allowed to be called
        /// and whether or not the element should be enabled or disabled
        /// </summary>
        //private bool CanExecute()
        //{
        //    // This will hinder the Execute() method to be called once the condition is falsy
        //    // BUT the UI does not get notified about the change and the button stays active
        //    return CanSave;
        //}

        public bool CanSave
        {
            get => _canSave;
            set => SetProperty(ref _canSave, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
    }
}