﻿using System;
using System.Windows;
using System.Windows.Controls;
using ModuleA.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace ModuleA
{
    public class ModuleAModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ModuleAModule(IRegionManager _regionManager)
        {
            this._regionManager = _regionManager;
        }

        /// <summary> Called after <see cref="RegisterTypes"/> and can be used to initialize the component
        /// using objects resolved from the container
        /// </summary>
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //RegisterViewWithViewDiscovery("ContentRegion", typeof(ViewA));
            RegisterViewWithViewInjection<ViewA>(containerProvider, "ContentRegion");
        }

        /// <summary>Is called first. It can be used to register types specific to this module</summary>
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //throw new System.NotImplementedException();
        }

        /// <summary>
        ///
        /// View injection:
        ///  shifts the responsibility to create and show the view to the developers
        ///  which gives them explicit control on how to add the view to the region
        ///  and activate/deactivate regions manually
        ///  regions must have been registered beforehand.
        /// </summary>
        private void RegisterViewWithViewInjection<TView>(
            IContainerProvider containerProvider, string regionName
        ) where TView : UserControl
        {
            var region = _regionManager.Regions[regionName];

            //  region.
            //      Add(viewInstance)
            //      Remove(viewInstance)
            //      Activate(viewInstance)
            //      Deactivate(viewInstance)
            var view1 = containerProvider.Resolve<TView>();
            region.Add(view1);

            var view2 = containerProvider.Resolve<TView>();
            view2.Content = new TextBlock()
            {
                Text = "Hello from View 2",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                FontSize = 46
            };
            
            // there are now 2 views within the region
            region.Add(view2);
            
            // region is empty => no view is activated
            // adding a new view deactivates the current one => the new view needs to be activated explicitly
            region.Activate(view2);

            region.Deactivate(view2);
            // screen is now empty because deactivating or removing the top view does not automatically
            // activate the next view underneith

            region.Activate(view1);
            // view1 is visible
        }

        /// <summary>
        /// Views added automatically by the Region.
        /// No explicit control about when, where or how the view is loaded
        /// </summary>
        private void RegisterViewWithViewDiscovery(string regionName, Type view)
        {
            _regionManager.RegisterViewWithRegion(regionName, view);
        }
    }
}