﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hello_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _buttonContent;


        public int ButtonContent
        {
            get => _buttonContent;
            set
            {
                _buttonContent = value;
                OnPropertyChanged(nameof(ButtonContent));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

       

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void IncrementDecrementHeightButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tag = ((Button) sender).Tag as string;
            IncrementDecrement(tag, 25, amount => this.Height += amount);
        }

        private void IncrementDecrementWidthButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tag = ((Button) sender).Tag as string;
            IncrementDecrement(tag, 25, amount => this.Width += amount);
        }

        private void IncrementDecrementCounterButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tag = ((Button) sender).Tag as string;
            IncrementDecrement(tag, 1, amount => this.ButtonContent += amount);
        }

        private void IncrementDecrement(string mode, int amount, Action<int> incrementDecrementFn)
        {
            switch (mode)
            {
                case "+":
                {
                    incrementDecrementFn(amount);
                    break;
                }
                case "-":
                {
                    incrementDecrementFn(amount * -1);
                    break;
                }
                default:
                    break;
            }
        }
    }
}